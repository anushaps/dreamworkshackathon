#include "Boid.h"
#include<math.h>
/* Define the functions below */

Boid::Boid(int x, int y, int xbound, int ybound,
        int     mboundaryPadding    ,
        float   mmaxSpeed           ,
        float   mmaxForce           ,
        float   mflockSepWeight     ,
        float   mflockAliWeight     ,
        float   mflockCohWeight     ,
        float   mflockSepRadius     ,
        float   mflockAliRadius     ,
        float   mflockCohRadius     )
{

    boundaryPadding = mboundaryPadding;
	maxSpeed = mmaxSpeed;	
	maxForce = 	mmaxForce;	
	flockSepWeight = mflockSepWeight;
	flockAliWeight  = mflockAliWeight;
	flockCohWeight = mflockCohWeight;
	flockSepRadius = mflockSepRadius;
	flockAliRadius = mflockAliRadius;
	flockCohRadius = mflockCohRadius;

	loc = Vec2f((float)x,(float)y);
	vel = Vec2f(0.02,0.02);
	orient = atan2(vel.x,vel.y);
	
}

bool Boid::equals(Boid b) {
	if(this->loc.x == b.loc.x && this->loc.y == b.loc.y && this->vel.x == b.vel.x && this->vel.y == b.vel.y)
		return true;
	return false;
}

// Method to update location
void Boid::update(vector<Boid> &boids) {

	Vec2f alignment = align(boids);
	Vec2f cohesion1 = cohesion(boids);
	Vec2f separation = separate(boids);
	
	this->vel.x += alignment.x + cohesion1.x + separation.x;
	this->vel.y += alignment.y + cohesion1.y + separation.y;
 	//this->vel.normalize();
 	
 	this->vel.x = alignment.x * flockAliWeight + cohesion1.x * flockCohWeight + separation.x * flockSepWeight;
	this->vel.y = alignment.y * flockAliWeight + cohesion1.y * flockCohWeight + separation.y * flockSepWeight;
	
	this->vel.normalize();
	
	this->loc.x += this->vel.x*0.001;
	this->loc.y += this->vel.y*0.001;
	
	
	//boundCheck(boundaryPadding);
	orient = (this->vel.y, this->vel.x);
	orient = (orient * 180)/3.14;
	
}

void Boid::seek(Vec2f target,float weight) {
}

void Boid::avoid(Vec2f target,float weight) {
}

void Boid::boundCheck(int padding) 
{
	if((this->loc.x > 200 - padding) || (this->loc.y > 200 - padding))
		orient = int(orient + 180.0) % 360;
	else if((this->loc.x < padding) || (this->loc.y < padding))
		orient = int(orient + 180.0) % 360;	
}


// A method that calculates a steering vector towards a target
Vec2f Boid::steer(Vec2f target) {
   Vec2f desired = target - this->loc;
   desired.normalize();
   desired = desired * maxSpeed;
   
   Vec2f steer = desired - this->vel;
   steer.normalize();
   steer = steer * maxForce;
   //cout<<steer.x<<","<<steer.y<<endl;
   return steer;
}
/*
   Vec2f desired = target - this->loc;
   Vec2f steer = Vec2f(0.0,0.0);
   float d = dist(desired,target);
   if(d > 0.0)
   {
		desired.normalize();
	   
		if( d < 100.0)
   		{
   			float val = maxSpeed*(d/100.0);	   
   			desired = desired * val;
   		}
   		else
   			desired = desired * maxSpeed;		
   
   		steer = desired - this->vel;
   		steer.normalize();
   		steer = steer * maxForce;
   	}
   	else 	
   //cout<<steer.x<<","<<steer.y<<endl;
   return steer;*/
void Boid::flock(vector<Boid> &boids) {
	
}

bool Boid::isHit(int x, int y, int radius) {
}

// Separation
// Method checks for nearby boids and steers away
Vec2f Boid::separate(vector<Boid> &boids) {
    Vec2f v = Vec2f(0.0, 0.0);
    int neighbourCount = 0;
    //cout<<this->loc.x<<" " <<this->loc.y<<endl;
    for(int i=0; i<boids.size(); i++) {
    	if(!equals(boids[i])) {
    		//cout<<boids[i].loc.x<<" " <<boids[i].loc.y<<endl;
    		if(dist(this->loc, boids[i].loc) < flockSepRadius) {
    			v.x += (boids[i].loc.x - this->loc.x);
    			v.y += (boids[i].loc.y - this->loc.y);
    			
    			neighbourCount++;
    		}
    	}
    }
    
    if (neighbourCount == 0)
    	{
    		return v;
    	}
    v.x /= neighbourCount;
	v.y /= neighbourCount;
	v.x *= -1;
	v.y *= -1;
	v.normalize();
	
	return v;
}

// Alignment
// For every nearby boid in the system, calculate the average velocity
Vec2f Boid::align(vector<Boid> &boids) {
    
    Vec2f v = Vec2f(0.0, 0.0);
    int neighbourCount = 0;
    
    for(int i=0; i<boids.size(); i++) {
    	if(!equals(boids[i])) {
    		if(dist(this->loc, boids[i].loc) < flockAliRadius) {
    			v.x += boids[i].vel.x;
    			v.y += boids[i].vel.y;
    			neighbourCount++;
    		}
    	}
    }
    
    if (neighbourCount == 0)
    	return v;
    
    v.x /= neighbourCount;
	v.y /= neighbourCount;
	v.normalize();
	return v;
}

// Cohesion
// For the average location (i.e. center) of all nearby boids, calculate steering vector towards that location
Vec2f Boid::cohesion(vector<Boid> &boids) {
    Vec2f v = Vec2f(0.0, 0.0);
    int neighbourCount = 0;
    
    for(int i=0; i<boids.size(); i++) {
    	if(!equals(boids[i])) {
    		if(dist(this->loc, boids[i].loc) < flockCohRadius) {
    			v.x += boids[i].loc.x;
    			v.y += boids[i].loc.y;
    			neighbourCount++;
    		}
    	}
    }
    
    if (neighbourCount == 0)
    	return v;
    
    v.x /= neighbourCount;
	v.y /= neighbourCount;
	//v = Vec2f(v.x - this->loc.x, v.y - this->loc.y);
//	v.normalize();
	return steer(v);
}


float Boid::dist(Vec2f v1,Vec2f v2)
{
	return sqrt(((v2.x-v1.x)*(v2.x-v1.x)) + ((v2.y-v1.y)*(v2.y-v1.y)));
}

float Boid::clamp(float val, float minval, float maxval)
{
    
}
