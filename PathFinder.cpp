#include "PathFinder.h"

/* Define the functions below */
std::vector<Vec2f>
PathFinder::getPath(Scene* scene)
{
	
}

Vec2f
PathFinder::getPath(Scene* scene, Vec2f currentPosition)
{
	Vec2f newPos = Vec2f(currentPosition.x, currentPosition.y);
	float** sdf = scene->getSDFhandle();
	Vec2f end = scene->getEndPosition();
	if( sdf[int(currentPosition.x)][int(currentPosition.y)] < 20) {
		float maxi = -1, maxj = -1, max = -1;
		for(int i = -1; i<2; i++)
		{
			for(int j = -1; j<2; j++) {
				if(sdf[int(currentPosition.x)+i][int(currentPosition.y)+j] > max) {
					maxi = i;
					maxj = j;
					max = sdf[int(currentPosition.x)+i][int(currentPosition.y)+j];
				}
			}
		}
		newPos = Vec2f(currentPosition.x+maxi,currentPosition.y+maxj); 
	}
	else {
		float slopex = end.x - currentPosition.x;
		float slopey = end.y - currentPosition.y;
		newPos = Vec2f(currentPosition.x+slopex/abs(slopex),currentPosition.y+slopey/abs(slopey)); 
	}
	return newPos;
	
}


SearchNode*
PathFinder::getNextNode()
{
	
}

void
PathFinder::searchNode(unsigned int x,unsigned int y, double movementCost, SearchNode* parent)
{
	
}

void
PathFinder::searchPath()
{
	
}

void
PathFinder::cleanup()
{
	
}
