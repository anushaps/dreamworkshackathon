#include "Flocking.h"

/* Define the functions below */

int Flocking::update()
{
	for(int i = 0; i<boids.size(); i++) {
		boids[i].update(boids);
		//boids[i].loc = pathFinder.getPath(sceneMap,boids[i].loc);
	}
	return 1;
    
}


void Flocking::addBoid(int x, int y)
{
	Boid boidVec = Boid(x,y,x_bound,y_bound,boundaryPadding,
   							maxSpeed,
   							maxForce,
   							flockSepWeight,
   							flockAliWeight,
   							flockCohWeight,
   							flockSepRadius,
   							flockAliRadius,
   							flockCohRadius);
   							
   boids.push_back(boidVec);
}

void Flocking::removeBoid(int x, int y, int radius)
{
   
}

int Flocking::flockSize()
{
	
}


void Flocking::setBounds(int x, int y)
{
	x_bound = x;
	y_bound = y;
}

void Flocking::setDestination(int x, int y,float area)
{
	destination = Vec2f((float)x,(float)y);
	destinationArea = area;
}


void Flocking::setDestination(Vec2f dest,float area)
{
	destination = dest;
    destinationArea = area;
}


void Flocking::setSceneMap(Scene* scene)
{
	sceneMap = scene;

}

void Flocking::useCollisionSDF(bool val)
{

}

vector<Boid> Flocking::getBoids()
{
	return boids;
}

Vec2f** Flocking::calculatePartialDerivaties()
{
	
		
}

void Flocking::setSimulationParameters(
    		int 	mboundaryPadding 	,
            float 	mmaxSpeed 			,
            float 	mmaxForce 			,
          	float 	mflockSepWeight 	,
          	float 	mflockAliWeight 	,
          	float 	mflockCohWeight 	,
          	float 	mcollisionWeight 	,
          	float 	mflockSepRadius 	,
          	float 	mflockAliRadius 	,
          	float 	mflockCohRadius 	,
          	float 	mdestWeight 		)
{
	boundaryPadding  = mboundaryPadding ;
			maxSpeed = mmaxSpeed 		;
			maxForce = mmaxForce 	;
			flockSepWeight  = mflockSepWeight ;
			flockAliWeight  = mflockAliWeight ;
			flockCohWeight  = mflockCohWeight ;
			collisionWeight = mcollisionWeight; 
			flockSepRadius = mflockSepRadius ;
			flockAliRadius  = mflockAliRadius;
			flockCohRadius  = mflockCohRadius;
			destWeight 	 = mdestWeight ;
}
